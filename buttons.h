/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__BUTTONS_H_
#define __DOOR_OPENER__BUTTONS_H_

extern int button1;
extern int last_button1_read_time;
extern int debounced_button1;

extern int button2;
extern int last_button2_read_time;
extern int debounced_button2;


void read_button(int &value, int &debounced_value, int input_pin, int &last_read_time);


#endif
