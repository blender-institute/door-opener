# Dr. Sybren's Door Opener

Opens doors by scanning NFC cards.

Built for & tested on an Arduino Nano with the following hardware:

- [NXP NP532 NFC RFID Module V3 kit](https://www.ebay.nl/itm/NXP-PN532-NFC-RFID-Module-V3-kit-reader-writer-HTC-Mifare-Arduino-Android/273066283528).
- [Blue IIC I2C TWI Serial LCD1602A Module Display](https://geek.wish.com/product/5a897449c373f914203f617f); the 16x2 character one.

Note that the software assumes that the Arduino is on the "inside", the display and NFC reader is on
the "outside", and that the Ardino and other door-controlling hardware is unreachable from the
"outside".

## License

Copyright (C) 2018 dr. Sybren A. Stüvel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


## Requirements

Requires the following libraries:

- [ElecHouse PN532](https://github.com/elechouse/PN532); clone the repo and move its contents into your libraries folder.
- [Arduino-LiquidCrystal-I2C-library](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library); just clone into your libraries folder.
- Add a file `admin_key.cpp` that defines the UID and UID length (4 or 7 bytes) of your admin key.
  An example can be found in `admin_key.h`.


## Schematics

EasyEDA was used to design the circuit and the PCB:

- [Circuit diagram](https://easyeda.com/editor#id=0f1e3d1a0ea948f4aec01ea92507130a)
- [PCB design](https://easyeda.com/editor#id=c2f38ff6379545879460392f1c06cf31)


## Compatibility

Works: ISO14443A MiFare cards, such as the Netherlands OV-Chipkaart and NFC tags.

Does not work: Phones that randomise their NFC ID.


## Contact

You can contact the author of this software at https://gitlab.com/blender-institute/door-opener.
