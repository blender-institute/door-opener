/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__PINOUT_H_
#define __DOOR_OPENER__PINOUT_H_

/* Connect the NFC reader as follows:
 * SDA → A4
 * SCL → A5
 */

#define ADMIN_MODE_LED_PIN 9
#define ERASE_MODE_LED_PIN 10
#define CARD_ACCEPT_FLASH_PIN 9
#define CARD_REJECT_FLASH_PIN 10

const int BUTTON_1_PIN = 3;
const int BUTTON_2_PIN = 4;

#endif
