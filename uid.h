/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__UID_H__
#define __DOOR_OPENER__UID_H__

class UID {
public:
    uint8_t value[7] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the UID
    uint8_t length;                              // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

    UID();
    UID(const UID &other);
    UID(const uint8_t value[7], uint8_t length);

    /* Hopefully constant-time UID comparison */
    bool operator==(const UID &other) const;
    bool operator!=(const UID &other) const;

    /* Read from NFC reader */
    bool read(bool fast=false);
    void print() const;

    /* Save to EEPROM */
    void save(uint32_t eeprom_offset) const;
    /* Load from EEPROM */
    void load(uint32_t eeprom_offset);
};

void setup_nfc();

#endif
