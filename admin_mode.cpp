/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>

#include "admin_mode.h"
#include "display.h"
#include "door.h"
#include "eeprom.h"
#include "pinout.h"
#include "timeouts.h"
#include "uid.h"


bool wait_for_admin_key(const __FlashStringHelper *title,
                        uint8_t status_led) {
    unsigned long last_used = millis();
    UID uid, last_read_uid;
    bool success;
    bool is_admin;

    analogWrite(status_led, 64);
    Serial.println(F("Present admin key."));

    while(true) {
        unsigned long idle_for = millis() - last_used;
        disp_present_admin_key(title, ADMIN_KEY_TIMEOUT - idle_for);

        success = uid.read();
        if (success) break;

        if (idle_for > ADMIN_KEY_TIMEOUT) {
            Serial.println(F("Timeout waiting for admin key."));
            digitalWrite(status_led, LOW);
            return false;
        }
    }
    digitalWrite(status_led, HIGH);

    last_read_uid = uid;
    is_admin = (uid == ADMIN_CARD);
    if (is_admin) {
        Serial.print(F("Admin key accepted"));
        disp_admin_key_accepted(title);
    }
    else {
        Serial.print(F("Wrong key presented"));
        disp_admin_key_rejected(title);
        delay(2000);
    }
    Serial.println(F(", remove key."));

    // Wait for key removal
    while(true) {
        success = uid.read(true);
        if (!success) break;
        if (uid != last_read_uid) break;
    }

    return is_admin;
}


static void exit_admin_mode() {
    digitalWrite(ADMIN_MODE_LED_PIN, LOW);
    keep_door_open(false);
    disp_idle();
}

void admin_mode() {
    UID uid, last_read_uid;
    boolean success;

    keep_door_open(true);

    if (!wait_for_admin_key(F("Learn Mode"), ADMIN_MODE_LED_PIN)) {
        exit_admin_mode();
        return;
    }
    Serial.println(F("ALL HAIL THE BIG ADMIN"));
    show_key_count();
    disp_admin_show_key_count(keys_in_eeprom());

    unsigned long last_used = millis();
    uint8_t seconds_left = 0;
    uint8_t last_seconds_left = 255;
    while(true) {
        success = uid.read();
        if (!success) {
            unsigned long idle_for = millis() - last_used;
            if (idle_for > ADMIN_MODE_TIMEOUT) {
                Serial.println(F("Admin mode timeout, resuming normal mode."));
                break;
            }

            disp_admin_present_key();
            seconds_left = (ERASE_MODE_TIMEOUT - idle_for) / 1000;
            if (seconds_left != last_seconds_left) {
                last_seconds_left = seconds_left;
                Serial.print(F("Waiting for card to grant access. Timeout in "));
                Serial.print(seconds_left);
                Serial.println(F(" sec"));
            }
            continue;
        }

        // Wait for the last-read card to be removed.
        if (uid == last_read_uid) {
            continue;
        }
        last_read_uid = uid;
        last_used = millis();

        if (eeprom_uid_has_access(uid)) {
            disp_admin_key_known();
            Serial.println(F("This card already has access, present another card."));
            digitalWrite(CARD_REJECT_FLASH_PIN, HIGH);
            delay(125);
            digitalWrite(CARD_REJECT_FLASH_PIN, LOW);
            delay(125);
            digitalWrite(CARD_REJECT_FLASH_PIN, HIGH);
            delay(125);
            digitalWrite(CARD_REJECT_FLASH_PIN, LOW);
            continue;
        }

        digitalWrite(ADMIN_MODE_LED_PIN, LOW);
        delay(250);
        digitalWrite(ADMIN_MODE_LED_PIN, HIGH);

        disp_admin_key_added();
        Serial.print(F("Granting access to: "));
        uid.print();
        Serial.println("");
        eeprom_append_uid(uid);
        disp_admin_show_key_count(keys_in_eeprom());
    }

    exit_admin_mode();
}
