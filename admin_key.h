/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__ADMIN_KEY_H__
#define __DOOR_OPENER__ADMIN_KEY_H__

#include <stdint.h>

extern const uint8_t admin_card_uid[7];
extern const uint8_t admin_card_uid_length;

/** Add this to a new file admin_key.cpp:
#include "admin_key.h"

const uint8_t admin_card_uid[7] = {0x11, 0x22, 0x33, 0x44, 0, 0, 0};
const uint8_t admin_card_uid_length = 4;
*/

#endif
