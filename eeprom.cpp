/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>
#include <EEPROM.h>

#include "display.h"
#include "eeprom.h"
#include "uid.h"


uint8_t keys_in_eeprom() {
    return EEPROM[2];
}

void keys_in_eeprom(uint8_t new_key_count) {
    EEPROM.update(2, new_key_count);
}

void eeprom_ensure_init() {
    if (EEPROM[0] == 0xc3 && EEPROM[1] == 0xbc) {
        return;
    }

    Serial.println(F("Magic bytes not found in EEPROM, initialising."));
    eeprom_init();
}

void eeprom_init() {
    /* Magic bytes */
    EEPROM.update(0, 0xc3);
    EEPROM.update(1, 0xbc);

    /* Number of keys stored */
    EEPROM.update(2, 0);
}

void eeprom_append_uid(const UID &uid) {
    uint8_t nr_of_keys = keys_in_eeprom();
    uint32_t eeprom_offset = EEPROM_HEADER_SIZE + nr_of_keys * EEPROM_KEY_SIZE;

    if (eeprom_offset + EEPROM_KEY_SIZE >= EEPROM.length()) {
        Serial.print(F("Unable to add more keys, EEPROM is full!"));
        return;
    }

    Serial.print(F("Writing to EEPROM offset "));
    Serial.print(eeprom_offset);
    Serial.println();
    uid.save(eeprom_offset);
    keys_in_eeprom(nr_of_keys + 1);
}

void eeprom_remove_uid(const UID &uid)
{
    uint8_t nr_of_keys = keys_in_eeprom();

    if (nr_of_keys == 0) {
        Serial.print(F("Unable to remove key, EEPROM is empty!"));
        return;
    }

    uint32_t last_key_eeprom_offset = EEPROM_HEADER_SIZE + (nr_of_keys-1) * EEPROM_KEY_SIZE;
    UID last_key;
    last_key.load(last_key_eeprom_offset);

    uint32_t eeprom_offset = EEPROM_HEADER_SIZE;
    UID uid_in_eeprom;

    for (uint8_t key_idx=0; key_idx < nr_of_keys; key_idx++, eeprom_offset += EEPROM_KEY_SIZE) {
        uid_in_eeprom.load(eeprom_offset);
        if (uid_in_eeprom != uid) {
            continue;
        }

        // Overwrite the key to remove with the last key in the EEPROM.
        Serial.print(F("Moving last key from EEPROM offset "));
        Serial.print(last_key_eeprom_offset);
        Serial.print(" to ");
        Serial.print(eeprom_offset);
        Serial.println();
        last_key.save(eeprom_offset);
        keys_in_eeprom(nr_of_keys - 1);
        break;
    }
}

bool eeprom_uid_has_access(const UID &uid_to_check) {
    uint8_t nr_of_keys = keys_in_eeprom();
    uint32_t eeprom_offset = EEPROM_HEADER_SIZE;
    UID uid_in_eeprom;

    for (uint8_t key_idx=0; key_idx < nr_of_keys; key_idx++, eeprom_offset += EEPROM_KEY_SIZE) {
        uid_in_eeprom.load(eeprom_offset);
        if (uid_in_eeprom == uid_to_check) {
            return true;
        }
    }
    return false;
}

void show_key_count() {
    Serial.print(F("EEPROM has space for "));
    Serial.print((EEPROM.length() - EEPROM_HEADER_SIZE) / EEPROM_KEY_SIZE);
    Serial.println(F(" keys"));
    Serial.print(F("EEPROM contains "));
    Serial.print(keys_in_eeprom());
    Serial.println(F(" keys"));
    disp_admin_show_key_count(keys_in_eeprom());
}

void eeprom_show_keys()
{
    Serial.print(F("EEPROM has space for "));
    Serial.print((EEPROM.length() - EEPROM_HEADER_SIZE) / EEPROM_KEY_SIZE);
    Serial.println(F(" keys"));
    Serial.println(F("EEPROM contains:"));

    uint8_t nr_of_keys = keys_in_eeprom();
    uint32_t eeprom_offset = EEPROM_HEADER_SIZE;
    UID uid;
    for (uint8_t key_idx=0; key_idx < nr_of_keys; key_idx++, eeprom_offset += EEPROM_KEY_SIZE) {
        uid.load(eeprom_offset);
        Serial.print("  - ");
        Serial.print(key_idx);
        Serial.print(" ");
        uid.print();
        Serial.println("");
    }
    Serial.println();
}
