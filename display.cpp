/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>

#include "display.h"
#include "timeouts.h"

#ifdef USE_DISPLAY
#  include <Wire.h>
#  include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);
#endif

uint8_t bell[8]  = {0x4, 0xe, 0xe, 0xe, 0x1f, 0x0, 0x4};
uint8_t note[8]  = {0x2, 0x3, 0x2, 0xe, 0x1e, 0xc, 0x0};
uint8_t clock[8] = {0x0, 0xe, 0x15, 0x17, 0x11, 0xe, 0x0};
uint8_t heart[8] = {0x0, 0xa, 0x1f, 0x1f, 0xe, 0x4, 0x0};
uint8_t duck[8]  = {0x0, 0xc, 0x1d, 0xf, 0xf, 0x6, 0x0};
uint8_t check[8] = {0x0, 0x1 ,0x3, 0x16, 0x1c, 0x8, 0x0};
uint8_t cross[8] = {0x0, 0x1b, 0xe, 0x4, 0xe, 0x1b, 0x0};
uint8_t retarrow[8] = {	0x1, 0x1, 0x5, 0x9, 0x1f, 0x8, 0x4};

uint8_t char_bell = 0;
uint8_t char_note = 1;
uint8_t char_clock = 2;
uint8_t char_heart = 3;
uint8_t char_duck = 4;
uint8_t char_check = 5;
uint8_t char_cross = 6;
uint8_t char_retarrow = 7;

unsigned long last_disp_call = 0; // time of last _disp_start() call, in  ms

void _2nd_line(const __FlashStringHelper *F);


void _disp_start(bool clear=true) {
#ifdef USE_DISPLAY
    if (clear) lcd.clear();
    lcd.home();
    lcd.setBacklight(HIGH);
    last_disp_call = millis();
#endif
}

void _twolines(
    const __FlashStringHelper *line1,
    const __FlashStringHelper *line2,
    int char1=-1, int char2=-1)
{
#ifdef USE_DISPLAY
    _disp_start();
    if (char1 >= 0) lcd.write((uint8_t)char1);
    lcd.print(line1);
    lcd.setCursor(0, 1);
    if (char2 >= 0) lcd.write((uint8_t)char2);
    lcd.print(line2);
#endif
}

void disp_setup() {
#ifdef USE_DISPLAY
    lcd.begin();
    _disp_start();
    lcd.write(char_clock);
    lcd.print(F(" Booting"));

	lcd.createChar(char_bell, bell);
	lcd.createChar(char_note, note);
	lcd.createChar(char_clock, clock);
	lcd.createChar(char_heart, heart);
	lcd.createChar(char_duck, duck);
	lcd.createChar(char_check, check);
	lcd.createChar(char_cross, cross);
	lcd.createChar(char_retarrow, retarrow);
#endif
}

void _disp_idle_message() {
    // Not using _twoline() to avoid influencing last_disp_call timeout.
    lcd.home();
    lcd.print(F("   Welcome to   "));
    lcd.setCursor(0, 1);
    lcd.print(F("    Blender     "));
}

void disp_loop() {
#ifdef USE_DISPLAY
    if (millis() - last_disp_call < DISP_TIMEOUT) return;

    lcd.setBacklight(LOW);
    _disp_idle_message();
#endif
}

void disp_idle() {
#ifdef USE_DISPLAY
    _disp_start();
    _disp_idle_message();
#endif
}

void disp_no_nfc() {
#ifdef USE_DISPLAY
    _twolines(
        F(" Unable to find"),
        F("  NFC reader"),
        char_bell);
#endif
}

void disp_off() {
#ifdef USE_DISPLAY
    lcd.clear();
    lcd.setBacklight(LOW);
#endif
}

void disp_access_granted() {
#ifdef USE_DISPLAY
    _disp_start();
    lcd.write(char_heart);
    lcd.print(F(" Access Granted"));
#endif
}

void disp_access_denied() {
#ifdef USE_DISPLAY
    _disp_start();
    lcd.write(char_cross);
    lcd.print(F(" Access DENIED"));
#endif
}

void disp_door_open() {
#ifdef USE_DISPLAY
    _disp_start(false);
    lcd.setCursor(0, 1);
    lcd.print(F("... OPENING ...."));
#endif
}
void disp_door_close() {
#ifdef USE_DISPLAY
    _disp_start(false);
    lcd.setCursor(0, 1);
    lcd.print(F("... CLOSING ...."));
#endif
}

void disp_present_admin_key(const __FlashStringHelper *title,
                            unsigned long time_left_ms) {
#ifdef USE_DISPLAY
    // Timeout should be shown after 1, not after 0.
    unsigned int secs_left = max(0, time_left_ms / 1000) + 1;

    _disp_start();
    lcd.print(title);
    lcd.setCursor(15, 0);
    lcd.print(secs_left);
    lcd.setCursor(0, 1);
    lcd.print(F("Scan admin key"));
#endif
}

void disp_admin_key_accepted(const __FlashStringHelper *title) {
    _twolines(title, F("Welcome ADMIN"));
}

void disp_admin_key_rejected(const __FlashStringHelper *title) {
    _twolines(title, F("KEY REJECTED"));
}


void disp_admin_show_key_count(uint8_t key_count) {
#ifdef USE_DISPLAY
    _disp_start();
    lcd.write(char_duck);
    lcd.print(' ');
    lcd.print(key_count);
    lcd.print(F(" keys known"));
#endif
}

void _2nd_line(const __FlashStringHelper *F) {
#ifdef USE_DISPLAY
    _disp_start(false);
    lcd.setCursor(0, 1);
    lcd.print(F);
#endif
}

void disp_admin_present_key() {
    _2nd_line(F("Scan key to add"));
}

void disp_admin_key_known() {
    _2nd_line(F("Already known..."));
}
void disp_admin_key_added() {
    _2nd_line(F("Key added......."));
}

void disp_erase_present_key() {
    _2nd_line(F("Scan key to DEL."));
}

void disp_erase_mode_start() {
    _twolines(F(" Release button"), F(" to avoid erase"), char_cross, char_cross);
}

void disp_erase_aborted() {
    _twolines(F(" Erase aborted"), F(" You are safe"), char_heart, char_heart);
}

void disp_erase_erasing() {
    _twolines(F(" ERASING ALL"), F(" STORED KEYS"), char_cross, char_cross);
}

void disp_erase_key_unknown()
{
    _2nd_line(F("Key unknown....."));
}
void disp_erase_key_removed()
{
    _2nd_line(F("Key removed....."));
}
