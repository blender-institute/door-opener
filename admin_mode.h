/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__ADMIN_MODE_H_
#define __DOOR_OPENER__ADMIN_MODE_H_

#include "admin_key.h"
#include "uid.h"

const UID ADMIN_CARD(admin_card_uid, admin_card_uid_length);

bool wait_for_admin_key(const __FlashStringHelper *title, uint8_t status_led);
void admin_mode();

#endif
