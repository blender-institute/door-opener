/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__EEPROM_H__
#define __DOOR_OPENER__EEPROM_H__

class UID;

/*
 * The EPROM contains the following layout:
 * 0:     Magic byte \xc3
 * 1:     Magic byte \xbc
 * 2:     Number of keys in the EEPROM
 * 3-F:   reserved for future use
 * 10     Length of key #0
 * 11-17  UID of key #0
 * 18     Length of key #1
 * 19-1F  UID of key #1
 * ....   repeated blocks of 8 bytes (1 length + 7 UID)
 */

const uint32_t EEPROM_HEADER_SIZE = 16;
const uint32_t EEPROM_KEY_SIZE = 8;

uint8_t keys_in_eeprom();
void keys_in_eeprom(uint8_t new_key_count);

void eeprom_ensure_init();
void eeprom_init();
void eeprom_append_uid(const UID &uid);
void eeprom_remove_uid(const UID &uid);
bool eeprom_uid_has_access(const UID &uid_to_check);

void show_key_count();
void eeprom_show_keys();

#endif
