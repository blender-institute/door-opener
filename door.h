/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__DOOR_H_
#define __DOOR_OPENER__DOOR_H_

const int DOOR_OPEN_PIN = A3;  // should be bi-directional pin (so not A6/A7)
const int DOOR_CLOSE_PIN = A6; // can be input-only
const int DOOR_LED_PIN = LED_BUILTIN;

void door_setup();

void door_open();
bool is_door_open();
void wait_for_door_close();
void show_door_status();

void keep_door_open(bool enable);

#endif
