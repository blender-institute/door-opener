/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>
#include <EEPROM.h>

#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <NfcAdapter.h>

#include "display.h"
#include "halt.h"
#include "uid.h"

PN532_I2C pn532i2c(Wire);
PN532 nfc(pn532i2c);

UID::UID() {
    length = 0;
    memset(value, 0, sizeof(value));
}
UID::UID(const UID &other) {
    length = other.length;
    memcpy(value, other.value, length);
}
UID::UID(const uint8_t new_val[7], uint8_t new_len) {
    length = new_len;
    memcpy(value, new_val, new_len);
}

void UID::print() const {
    for (uint8_t i=0; i < length; i++) {
        if (i) Serial.write(':');
        if (value[i] < 0x10) Serial.write('0');
        Serial.print(value[i], HEX);
    }
}

bool UID::operator==(const UID &other) const {
    bool correct = (length == other.length);
    for (int idx=0; idx < length; idx++) {
        correct &= (value[idx] == other.value[idx]);
    }
    return correct;
}
bool UID::operator!=(const UID &other) const {
    return !(*this == other);
}

bool UID::read(bool fast) {
    nfc.setPassiveActivationRetries(fast ? 0x01 : 0x20);

    // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
    // 'uid' will be populated with the UID, and uidLength will indicate
    // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
    boolean success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, value, &length);
    if (!success) return false;

    if (length > sizeof(value)) {
        Serial.print(F("Read UID of size "));
        Serial.print(length);
        Serial.print(F(", memory corrupt. DYING."));
        halt();
    }

    return true;
}

void UID::save(uint32_t eeprom_offset) const {
    EEPROM[eeprom_offset] = length;
    for (int idx=0; idx < length; idx++) {
        /* Only write to the EEPROM if the value is different, saves write cycles */
        EEPROM.update(eeprom_offset + 1 + idx, value[idx]);
    }
}
void UID::load(uint32_t eeprom_offset) {
    length = EEPROM[eeprom_offset];
    for (int idx=0; idx < length; idx++) {
        value[idx] = EEPROM[eeprom_offset + 1 + idx];
    }
}


void setup_nfc() {
    Serial.println(F("Setting up NFC"));
    nfc.begin();

    Serial.println(F("Obtaining firmware version"));
    uint32_t versiondata = nfc.getFirmwareVersion();
    if (! versiondata) {
        disp_no_nfc();
        Serial.println("Didn't find PN53x board");
        halt();
        return;
    }

    // Got ok data, print it out!
    Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
    Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
    Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);

    // Set the max number of retry attempts to read from a card
    // This prevents us from waiting forever for a card, which is
    // the default behaviour of the PN532.
    nfc.setPassiveActivationRetries(0xFE);

    // configure board to read RFID tags
    nfc.SAMConfig();
}
