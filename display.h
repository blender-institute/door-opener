/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#ifndef __DOOR_OPENER__DISPLAY_H_HH
#define __DOOR_OPENER__DISPLAY_H_HH

/**
 * I2C Display support.
 *
 * Based on the library at https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
 */

#define USE_DISPLAY  // comment out to not use the i2c display

void disp_setup();
void disp_loop();

void disp_idle();
void disp_no_nfc();
void disp_off();

void disp_access_granted();
void disp_access_denied();

void disp_door_open();
void disp_door_close();

void disp_present_admin_key(const __FlashStringHelper *title, unsigned long time_left_ms);
void disp_admin_key_accepted(const __FlashStringHelper *title);
void disp_admin_key_rejected(const __FlashStringHelper *title);

void disp_admin_present_key();
void disp_admin_show_key_count(uint8_t key_count);
void disp_admin_key_known();
void disp_admin_key_added();

void disp_erase_mode_start();
void disp_erase_present_key();
void disp_erase_aborted();
void disp_erase_erasing();
void disp_erase_key_unknown();
void disp_erase_key_removed();

#endif
