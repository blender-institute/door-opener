/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
/**
 * NFC door opener.
 *
 * The hard-coded ADMIN_CARD will enable admin mode. Once in admin mode,
 * the next card read will be granted access to open the door.
 *
 */

#include "admin_mode.h"
#include "admin_key.h"
#include "buttons.h"
#include "display.h"
#include "eeprom.h"
#include "door.h"
#include "erase_mode.h"
#include "pinout.h"
#include "timeouts.h"
#include "uid.h"


void setup() {
    Serial.begin(115200);
    disp_setup();

    Serial.println(F("Yay! This is Sybren's door opener"));
    door_setup();
    pinMode(ADMIN_MODE_LED_PIN, OUTPUT);
    pinMode(ERASE_MODE_LED_PIN, OUTPUT);

    pinMode(BUTTON_1_PIN, INPUT_PULLUP);
    pinMode(BUTTON_2_PIN, INPUT_PULLUP);

    Serial.println(F("I/O pins configured."));

    eeprom_ensure_init();
    Serial.print(keys_in_eeprom());
    Serial.println(F(" keys have access."));
    eeprom_show_keys();

    setup_nfc();
    Serial.println(F("Ready for operation."));

    for (int i=0; i<3; i++) {
        digitalWrite(CARD_ACCEPT_FLASH_PIN, HIGH);
        delay(100);
        digitalWrite(CARD_ACCEPT_FLASH_PIN, LOW);
        delay(100);
    }

    disp_idle();
}


void loop() {
    UID uid;
    boolean success;

    // Turns off backlight and shows idle status after a delay.
    disp_loop();

    show_door_status();

    /* TODO: debounce in hardware and use interrupts instead of polling */
    read_button(button1, debounced_button1, BUTTON_1_PIN, last_button1_read_time);
    read_button(button2, debounced_button2, BUTTON_2_PIN, last_button2_read_time);

    success = uid.read();
    if (!success) {
        // TODO: ping watchdog hardware once we have that.
        return;
    }

    Serial.print(F("CARD: "));
    uid.print();
    Serial.println("");

    if (eeprom_uid_has_access(uid)) {
        disp_access_granted();
        digitalWrite(CARD_ACCEPT_FLASH_PIN, HIGH);
        door_open();
        delay(100);
        digitalWrite(CARD_ACCEPT_FLASH_PIN, LOW);
        wait_for_door_close();
    }
    else {
        disp_access_denied();
        digitalWrite(CARD_REJECT_FLASH_PIN, HIGH);
        delay(100);
        digitalWrite(CARD_REJECT_FLASH_PIN, LOW);
    }

    // Wait 1 second before continuing
    delay(1000);
    disp_idle();
}
