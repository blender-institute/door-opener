/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>

#include "admin_mode.h"
#include "display.h"
#include "door.h"
#include "eeprom.h"
#include "pinout.h"
#include "timeouts.h"
#include "uid.h"

static void exit_erase_mode() {
    digitalWrite(ERASE_MODE_LED_PIN, LOW);
    keep_door_open(false);
    disp_idle();
}

void erase_mode() {
    UID uid, last_read_uid;
    bool success;

    keep_door_open(true);

    Serial.println(F("Present admin key to enable erase mode."));
    Serial.println(F("While in erase mode, present keys to revoke access."));
    Serial.println(F("Note that this does *not* securely erase keys."));
    Serial.println(F("It just affects normal operation of the door."));
    disp_erase_mode_start();

    for (int i=0; i<20; i++) {
        digitalWrite(ERASE_MODE_LED_PIN, HIGH);
        delay(100);
        digitalWrite(ERASE_MODE_LED_PIN, LOW);
        delay(100);
    }

    bool still_pressed = (digitalRead(BUTTON_2_PIN) == 0);
    if (!still_pressed) {
        Serial.println(F("Erase button was released, returning to normal mode."));
        disp_erase_aborted();
        delay(2000);
        exit_erase_mode();
        return;
    }

    if (!wait_for_admin_key(F("Erase Mode"), ERASE_MODE_LED_PIN)) {
        disp_erase_aborted();
        delay(2000);
        exit_erase_mode();
        return;
    }

    show_key_count();
    disp_admin_show_key_count(keys_in_eeprom());

    unsigned long last_used = millis();
    uint8_t seconds_left = 0;
    uint8_t last_seconds_left = 255;
    while(true) {
        success = uid.read();
        if (!success) {
            unsigned long idle_for = millis() - last_used;
            if (idle_for > ERASE_MODE_TIMEOUT) {
                Serial.println(F("Erase mode timeout, resuming normal mode."));
                break;
            }

            disp_erase_present_key();
            seconds_left = (ERASE_MODE_TIMEOUT - idle_for) / 1000;
            if (seconds_left != last_seconds_left) {
                last_seconds_left = seconds_left;
                Serial.print(F("Waiting for card to erase access. Timeout in "));
                Serial.print(seconds_left);
                Serial.println(F(" sec"));
            }
            continue;
        }

        // Wait for the last-read card to be removed.
        if (uid == last_read_uid) {
            continue;
        }
        last_read_uid = uid;
        last_used = millis();

        if (!eeprom_uid_has_access(uid)) {
            disp_erase_key_unknown();
            Serial.println(F("This card is unknown, present another card."));
            continue;
        }

        digitalWrite(ERASE_MODE_LED_PIN, LOW);
        delay(250);
        digitalWrite(ERASE_MODE_LED_PIN, HIGH);

        disp_erase_key_removed();
        eeprom_remove_uid(uid);
        Serial.print(F("Revoked access to: "));
        uid.print();
        Serial.println("");
        disp_admin_show_key_count(keys_in_eeprom());
    }

    exit_erase_mode();
}
