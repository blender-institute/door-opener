/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>

#include "admin_mode.h"
#include "buttons.h"
#include "erase_mode.h"
#include "pinout.h"


const int DEBOUNCE_DIGITAL_PIN_TIME = 50; // milliseconds


// Digital inputs.
int button1 = 0;
int last_button1_read_time = 0;
int debounced_button1 = 0;

int button2 = 0;
int last_button2_read_time = 0;
int debounced_button2 = 0;


static void button_value_changed(int value, int input_pin) {
    switch (input_pin) {
    case BUTTON_1_PIN:
        if (!value) admin_mode();
        break;
    case BUTTON_2_PIN:
        if (!value) erase_mode();
        break;
    }
}

void read_button(int &value, int &debounced_value, int input_pin, int &last_read_time) {
    // read the state of the switch into a local variable:
    int reading = digitalRead(input_pin);
    // Serial.print("Button ");
    // Serial.print(input_pin);
    // Serial.print(" value ");
    // Serial.print(reading);
    // Serial.println("");

    // check to see if you just pressed the button
    // (i.e. the input went from LOW to HIGH),  and you've waited
    // long enough since the last press to ignore any noise:

    // If the switch changed, due to noise or pressing:
    if (reading != value) {
        // reset the debouncing timer
        last_read_time = millis();
    }

    if ((millis() - last_read_time) > DEBOUNCE_DIGITAL_PIN_TIME) {
        // whatever the reading is at, it's been there for longer
        // than the debounce delay, so take it as the actual current state:

        // if the button state has changed:
        if (reading != debounced_value) {
            debounced_value = value = reading;
            button_value_changed(debounced_value, input_pin);
        }
    }

    value = reading;
}
