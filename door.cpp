/* This file is part of Dr. Sybren's Door Opener.

Dr. Sybren's Door Opener is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your
option) any later version.

Dr. Sybren's Door Opener is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public
License along with Dr. Sybren's Door Opener.  If not, see
<https://www.gnu.org/licenses/>.
*/
#include <Arduino.h>

#include "display.h"
#include "door.h"
#include "timeouts.h"


void door_setup() {
    pinMode(DOOR_OPEN_PIN, INPUT_PULLUP);
    pinMode(DOOR_CLOSE_PIN, INPUT_PULLUP);
    pinMode(DOOR_LED_PIN, OUTPUT);
}

void door_open() {
    disp_door_open();
    Serial.println("Door open");
    digitalWrite(DOOR_LED_PIN, HIGH);

    pinMode(DOOR_OPEN_PIN, OUTPUT);
    analogWrite(DOOR_OPEN_PIN, 0);
    delay(100);
    pinMode(DOOR_OPEN_PIN, INPUT_PULLUP);

    digitalWrite(DOOR_LED_PIN, LOW);
}

void keep_door_open(bool enable)
{
    if (enable) {
        Serial.println("Keeping door open");
        digitalWrite(DOOR_LED_PIN, HIGH);
        pinMode(DOOR_OPEN_PIN, OUTPUT);
        analogWrite(DOOR_OPEN_PIN, 0);
    }
    else {
        Serial.println("Stopping keeping the door open");
        pinMode(DOOR_OPEN_PIN, INPUT_PULLUP);
        digitalWrite(DOOR_LED_PIN, LOW);
    }
}

bool is_door_open() {
    /* Low means 'open' or 'opening', high means 'closed' or 'closing'.
     * The threshold is rather low, at around 0.2 volt when the pin is
     * floating. */
    int sync_value = analogRead(DOOR_CLOSE_PIN);
    return sync_value < 30;
}

void wait_for_door_close() {
    Serial.println("Waiting for door to close");
    unsigned long wait_start = millis();

    while (millis() - wait_start < WAIT_DOOR_CLOSE_TIMEOUT) {
        if (!is_door_open()) {
            disp_door_close();
            Serial.println("Door closed");
            return;
        }

        delay(250);
    }

    Serial.println("Door did not close before timeout, stopping the wait.");
}


void show_door_status() {
    static unsigned long last_status_change = 0;
    static bool msg_showing = false;
    static bool door_was_open = false;
    bool door_is_open = is_door_open();

    if (door_was_open == door_is_open) {
        if (msg_showing && millis() - last_status_change > DOOR_STATUS_MSG_TIMEOUT) {
            disp_idle();
            msg_showing = false;
        }

        return;  // No status change
    }
    door_was_open = door_is_open;
    last_status_change = millis();
    msg_showing = true;

    if (door_is_open) {
        disp_door_open();
    }
    else {
        disp_door_close();
    }
}
